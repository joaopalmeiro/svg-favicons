# Notes

- https://web.dev/articles/building/an-adaptive-favicon
- https://austingil.com/svg-favicons/
- https://fav.farm/: `<link rel="icon" href="data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' width='48' height='48' viewBox='0 0 16 16'><text x='0' y='14'>😽</text></svg>" />`
- https://fav.farm/%F0%9F%91%93
- https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs
- https://emojidb.org/
- https://marketplace.visualstudio.com/items?itemName=jock.svg
- https://github.com/vitejs/vite/blob/create-vite%405.5.2/packages/create-vite/template-react-ts/index.html: `<link rel="icon" type="image/svg+xml" href="/vite.svg" />`
